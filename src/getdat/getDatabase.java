package getdat;
import com.mongodb.*;

import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

public class getDatabase {
	public static String getUserdata(String usd,String pwd){ //get role forn user data
		String role="";
		DBCollection coll;
		BasicDBObject query;
		Mongo m;
		DB db;
		try{
			m = getMongoConnection();
			db = m.getDB( "gts" );
			coll = db.getCollection("userdata");
			query = new BasicDBObject();
			query.put("username", usd);
			DBCursor cur = coll.find(query);
			boolean isrsnull=cur.hasNext();
			
			if(!isrsnull){
				System.out.println("not find data for username  : "+usd);
			}else{
				DBObject myDoc =cur.next();
				String pass =(String) myDoc.get("password");
				if(pwd.equals(pass)){
					role = (String) myDoc.get("role");
				}else{
					System.out.println("wrong password");
				}
			}
			m.close();
			cur.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return role;
	}
	public static DBObject getCompanyData(String username){ //get DBobject company collection by username 
		DBObject comdat=null;
		DBCollection coll;
		BasicDBObject query;
		Mongo m;
		DB db;
		try{
			m = getMongoConnection();
			db = m.getDB( "gts" );
			coll = db.getCollection("company");
			query = new BasicDBObject();
			query.put("username", username);
			DBCursor cur = coll.find(query);
			boolean isrsnull=cur.hasNext();
			
			if(!isrsnull){
				System.out.println("not find data for username  : "+username);
			}else{
				 comdat =cur.next();				
			}
			m.close();
			cur.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return comdat;
	}
	public static DBObject[] getVehicleCompany(String company_id,int row){ // get vehicle data by company ID one data for one array of DBobject
		DBCollection coll;
		BasicDBObject query;
		Mongo m;
		DB db;
		DBObject[] Vdat =new DBObject[row+1];
		try{
			m = getMongoConnection();
			db = m.getDB( "gts" );
			coll = db.getCollection("vehicle");
			query = new BasicDBObject();
			query.put("Company_ID", company_id);
			DBCursor cur = coll.find(query);
			boolean isrsnull=cur.hasNext();
			
			if(!isrsnull){
				System.out.println("not find data for Company ID  : "+company_id);
			}else{
				 for(int i=1;i<=row;i++){
					 Vdat[i]=cur.next();
				 }
			}
			m.close();
			cur.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return Vdat;
	}
	public static DBObject[] getDriverCompany(String company_id,int row){ // get Driver data by company ID one data for one array of DBobject
		DBCollection coll;
		BasicDBObject query;
		Mongo m;
		DB db;
		DBObject[] Vdat =new DBObject[row+1];
		try{
			m = getMongoConnection();
			db = m.getDB( "gts" );
			coll = db.getCollection("driver");
			query = new BasicDBObject();
			query.put("Company_ID", company_id);
			DBCursor cur = coll.find(query);
			boolean isrsnull=cur.hasNext();
			
			if(!isrsnull){
				System.out.println("not find data for Company ID  : "+company_id);
			}else{
				 for(int i=1;i<=row;i++){
					 Vdat[i]=cur.next();
				 }
			}
			m.close();
			cur.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return Vdat;
	}
	public static DBObject[] getDeviceCompany(String company_id,int row){ // get Driver data by company ID one data for one array of DBobject
		DBCollection coll;
		BasicDBObject query;
		Mongo m;
		DB db;
		DBObject[] Vdat =new DBObject[row+1];
		try{
			m = getMongoConnection();
			db = m.getDB( "gts" );
			coll = db.getCollection("device");
			query = new BasicDBObject();
			query.put("Company_ID", company_id);
			DBCursor cur = coll.find(query);
			boolean isrsnull=cur.hasNext();
			
			if(!isrsnull){
				System.out.println("not find data for Company ID  : "+company_id);
			}else{
				 for(int i=1;i<=row;i++){
					 Vdat[i]=cur.next();
				 }
			}
			m.close();
			cur.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return Vdat;
	}
	public static DBObject[] getCollData(String collection,String fild,String dat,int row){ // get Driver data by company ID one data for one array of DBobject
		DBCollection coll;
		BasicDBObject query;
		Mongo m;
		DB db;
		DBObject[] Vdat =new DBObject[row+1];
		try{
			m = getMongoConnection();
			db = m.getDB( "gts" );
			coll = db.getCollection(collection);
			query = new BasicDBObject();
			query.put(fild, dat);
			DBCursor cur = coll.find(query);
			boolean isrsnull=cur.hasNext();
			
			if(!isrsnull){
				System.out.println("not find data in collection : "+collection);
			}else{
				 for(int i=1;i<=row;i++){
					 Vdat[i]=cur.next();
				 }
			}
			m.close();
			cur.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return Vdat;
	}
	public static DBObject[] getCollData(String collection,String fild,String dat,int row ,boolean sort,String sortby,int order){ // get Driver data by company ID one data for one array of DBobject
		DBCollection coll;
		BasicDBObject query;
		BasicDBObject query2 = null;
		Mongo m;
		DB db;
		DBObject[] Vdat =new DBObject[row+1];
		try{
			m = getMongoConnection();
			db = m.getDB( "gts" );
			coll = db.getCollection(collection);
			query = new BasicDBObject();
			query2 = new BasicDBObject();
			query.put(fild, dat);
			DBCursor cur=null;
			if(sort){
				query2.put(sortby, order);
				cur = coll.find(query).sort(query2);
			}else{
			cur = coll.find(query);
			}
			boolean isrsnull=cur.hasNext();
			
			if(!isrsnull){
				System.out.println("not find data in collection : "+collection);
			}else{
				 for(int i=1;i<=row;i++){
					 Vdat[i]=cur.next();
				 }
			}
			m.close();
			cur.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return Vdat;
	}
	public static DBObject[] getCollData(String collection,BasicDBObject query,int row){ // get Driver data by company ID one data for one array of DBobject
		DBCollection coll;		
		Mongo m;
		DB db;
		DBObject[] Vdat =new DBObject[row+1];
		try{
			m = getMongoConnection();
			db = m.getDB( "gts" );
			coll = db.getCollection(collection);						
			DBCursor cur = coll.find(query);
			boolean isrsnull=cur.hasNext();
			
			if(!isrsnull){
				System.out.println("not find data in collection : "+collection);
				Vdat=null;
			}else{
				 for(int i=1;i<=row;i++){
					 Vdat[i]=cur.next();
				 }
			}
			m.close();
			cur.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return Vdat;
	}
	public static DBObject getCollData(String collection,String fild,String dat){ // get Driver data by company ID one data for one array of DBobject
		DBCollection coll;
		BasicDBObject query;
		Mongo m;
		DB db;
		DBObject Vdat =null;
		try{
			m = getMongoConnection();
			db = m.getDB( "gts" );
			coll = db.getCollection(collection);
			query = new BasicDBObject();
			query.put(fild, dat);
			DBCursor cur = coll.find(query);
			boolean isrsnull=cur.hasNext();
			
			if(!isrsnull){
				System.out.println("not find data in collection : "+collection);
			}else{
				  Vdat=cur.next();
				 
			}
			m.close();
			cur.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return Vdat;
	}
	public static int getVehicleCompanyRow(String company_id){ //get number of vehicle by company ID
		DBCollection coll;
		BasicDBObject query;
		Mongo m;
		DB db;
		int row =0;
		try{
			m = getMongoConnection();
			db = m.getDB( "gts" );
			coll = db.getCollection("vehicle");
			query = new BasicDBObject();
			query.put("Company_ID", company_id);
			DBCursor cur = coll.find(query);
			boolean isrsnull=cur.hasNext();
			
			if(!isrsnull){
				System.out.println("not find data for company ID in row  : "+company_id);
			}else{
				 row =cur.count();				
			}
			m.close();
			cur.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return row;
	}
	public static int getRowCount(String collection){// get row count of collection
		DBCollection coll;
		Mongo m;
		DB db;
		int row =0;
		try{
			m = getMongoConnection();
			db = m.getDB( "gts" );
			coll = db.getCollection(collection);
			DBCursor cur = coll.find();
			boolean isrsnull=cur.hasNext();
			
			if(!isrsnull){
				System.out.println("no row");
			}else{
				 row =cur.count();				
			}
			m.close();
			cur.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return row;
	}
	public static int getRowCount(String collection,String fild,String dat){ // get rowcount of collection have that data in a fild
		DBCollection coll;
		Mongo m;
		DB db;
		int row =0;
		BasicDBObject query= new BasicDBObject();
		try{
			m = getMongoConnection();
			db = m.getDB( "gts" );
			coll = db.getCollection(collection);
			query.put(fild, dat);
			DBCursor cur = coll.find(query);
			boolean isrsnull=cur.hasNext();
			
			if(!isrsnull){
				System.out.println("no row");
			}else{
				 row =cur.count();				
			}
			m.close();
			cur.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return row;
	}
	public static int getRowCount(String collection,BasicDBObject query){ // get rowcount of collection have that data in a fild
		DBCollection coll;
		Mongo m;
		DB db;
		int row =0;		
		try{
			m = getMongoConnection();
			db = m.getDB( "gts" );
			coll = db.getCollection(collection);			
			DBCursor cur = coll.find(query);
			boolean isrsnull=cur.hasNext();
			
			if(!isrsnull){
				System.out.println("no row");
			}else{
				 row =cur.count();				
			}
			m.close();
			cur.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return row;
	}
	public static boolean checkDupdata(String collection,String n_dat,String dat){// check dup in collection 
		
		boolean flag=false;
		DBCollection coll;
		BasicDBObject query;
		Mongo m;
		DB db;
		int row =0;
		try{
			m = getMongoConnection();
			db = m.getDB( "gts" );
			coll = db.getCollection(collection);
			query = new BasicDBObject();
			query.put(n_dat, dat);
			DBCursor cur = coll.find(query);
			flag=cur.hasNext();
			m.close();
			cur.close();		
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return flag;
	}
	public static boolean getDevice_company(String usn,String pwd,String[] device_id,String company_id,String stat){// use for add device only
		DBCollection coll;
		Mongo m;
		DB db;
		int row =0;
		boolean flag=false;
		String user_stat = getUserdata(usn,pwd);
		BasicDBObject query= new BasicDBObject();
		if(user_stat.equals("null")){
			System.out.println("invalid usn and pwd");
			
		}else{
			try{
				m = getMongoConnection();
				db = m.getDB( "gts" );
				coll = db.getCollection("device");
				query.put("Company_ID", company_id);
				query.put("stat", "0");
				DBCursor cur = coll.find(query);
				
				boolean isrs2null=cur.hasNext();
				if(!isrs2null){
					System.out.println("don't find vehicle with that company id");
				}else{
					
					int i=1;
					while(cur.hasNext()){
						device_id[i] = (String) cur.next().get("Device_ID");
						i=i+1;
					}
					flag= true;
					
				}
				m.close();
				cur.close();
			
			}catch(Exception e){
				e.printStackTrace();
			}
			
		}
		return flag;
	}
	public static String getActiveDeliverID(String vehicle_id){
		String deliver_id="";
		DBCollection coll;
		Mongo m;
		DB db;
		int row =0;
		BasicDBObject query= new BasicDBObject();
		try{
			m = getMongoConnection();
			db = m.getDB( "gts" );
			coll = db.getCollection("deliver");
			query.put("Vehicle_ID", vehicle_id);
			query.put("stat", "1");
			DBCursor cur = coll.find(query);
			
			boolean isrs2null=cur.hasNext();
			if(!isrs2null){
				System.out.println("don't find deliver with that vehicle id");
				deliver_id="notactive";
			}else{
				
				
				while(cur.hasNext()){
					deliver_id = (String) cur.next().get("Deliver_ID");
					
				}
				
				
			}
			m.close();
			cur.close();
		
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return deliver_id;
	}
	public static Mongo getMongoConnection() throws UnknownHostException, MongoException{
		Mongo m = new Mongo( "localhost" , 8081 );
		return m;
	}
	public static String[] geteventdata(double[] lat,double[] lon,String deliver_id,String vehicle_id){
		DBCollection coll;
		Mongo m;
		DB db;
		BasicDBObject query= new BasicDBObject();
		BasicDBObject query2= new BasicDBObject();
		DBObject eventData =null;
		String[] time= new String[lat.length];
		try{
			
			m = getMongoConnection();
			db = m.getDB( "gts" );
			coll = db.getCollection("eventdata");
			query.put("Deliver_ID", deliver_id);
			query.put("Vehicle_ID", vehicle_id);
			query2.put("timestamp", 1);
			DBCursor cur = coll.find(query).sort(query2);
			
	    if(!cur.hasNext()){
	    	System.out.println("no eventdata");
	    	
	    }else{
	       	    	
	    	int i=1;
	    	while(cur.hasNext()){
	    		eventData=cur.next();
	    		time[i]= (String) eventData.get("timestamp");
	    		lat[i]=Double.parseDouble((String)eventData.get("lat"));
	    		lon[i]=Double.parseDouble((String)eventData.get("lon"));
	    		i++;
	    	}
	    }
	    m.close();
		cur.close();
	    
		}catch (Exception e) {
			
			e.printStackTrace();
		}
		return time;
		
	}
	public static DBObject[] geteventdata(String vehicle_id,int row){
		DBCollection coll;
		Mongo m;
		DB db;
		BasicDBObject query= new BasicDBObject();
		BasicDBObject query2= new BasicDBObject();
		DBObject[] eventData =new DBObject[row+1];
		try{
			
			m = getMongoConnection();
			db = m.getDB( "gts" );
			coll = db.getCollection("eventdata");
			query.put("Vehicle_ID", vehicle_id);
			query2.put("timestamp", -1);
			DBCursor cur = coll.find(query).sort(query2);
	    
	    if(!cur.hasNext()){
	    	System.out.println("no eventdata");
	    	
	    }else{
	       	    	
	    	int i=1;
	    	while(cur.hasNext()){
	    		eventData[i]=cur.next();
 		
	    		i++;
	    	}
	    }
	    m.close();
		cur.close();
	    
		}catch (Exception e) {
			
			e.printStackTrace();
		}
		return eventData;
	}
	
}
