package raw_material;

import getdat.getDatabase;
import insDB.PutDB;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Add_raw extends HttpServlet {	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp,false);
	}	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp,true);
	}
	public void doWork(HttpServletRequest req, HttpServletResponse resp,boolean isPost)
			throws ServletException, IOException {
		HttpSession ses = req.getSession();
		String usn = (String) ses.getAttribute("usn");
		String pwd = (String) ses.getAttribute("pwd");
		String user_stat = (String) ses.getAttribute("user_stat");
		if(user_stat.equals("")||user_stat==null){
			
			String wr="Wrong ID or Password";
			req.setAttribute("wrong", wr);
			RequestDispatcher view = req.getRequestDispatcher("start.jsp?note="+wr+"");
			view.forward(req, resp);
						    
			}
		
		resp.setContentType("text/html;charset=utf-8");
		PrintWriter out =resp.getWriter();
		
			//out.println("<%@ page language=\"java\" contentType=\"text/html; charset=utf-8\"pageEncoding=\"utf-8\"%>");
			out.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">");
			out.println("<html>");
			out.println("<head>");
			out.println("<script type=\"text/javascript\">");
			out.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
			out.println("</script>");
			out.println("</head>");
			usn=(String) ses.getAttribute("usn");
			pwd=(String) ses.getAttribute("pwd");
			user_stat=(String) ses.getAttribute("user_stat");
			String company_id=(String) ses.getAttribute("company_id");			
			boolean dp=getDatabase.checkDupdata("manufacture", "Manufacture_ID", req.getParameter("p_id"));
			if(req.getParameter("p_name").equalsIgnoreCase("")){
				out.println("����ժ����Թ��ҷ��м�Ե");
			}else if(req.getParameter("p_count").equalsIgnoreCase("")){
				out.println("����ըӹǹ�Թ��ҷ��м�Ե");
			}else if(req.getParameter("p_id").equalsIgnoreCase("")){
				out.println("��������ʼ�Ե�Թ���");
			}else if(dp){
				out.println("���ʼ�Ե�Թ��ҫ��");
			}
			else{
				boolean f1=false;
				f1=PutDB.ins_manufacture(company_id, req.getParameter("p_id"), req.getParameter("p_name"),req.getParameter("p_count"),req.getParameter("order_day")+"",req.getParameter("order_month")+"",req.getParameter("order_year")+"",req.getParameter("in_day")+"",req.getParameter("in_month")+"",req.getParameter("in_year")+"",req.getParameter("p_warehouse")+"");
				if(f1){
					int nm=Integer.parseInt(req.getParameter("n_list"));
					boolean f2=false;
					for(int i=1;i<=nm;i++){
						    f2=PutDB.ins_Material(company_id, req.getParameter("p_id"), req.getParameter("m_num_"+i),
							req.getParameter("m_name_"+i), req.getParameter("m_des_"+i),
							req.getParameter("m_count_"+i), req.getParameter("m_w_"+i),
							req.getParameter("m_v_"+i), req.getParameter("m_price_"+i),req.getParameter("m_seller_"+i));
						    out.println(req.getParameter("m_num_"+i));
					}
					if(f2){
						out.println("Add manufacture ����");
						RequestDispatcher view = req.getRequestDispatcher("./read_buy_raw.jsp");
						view.forward(req, resp);
					}else{
						out.println("Add material Failed");
					}
				}else{
					out.println("Add manufacture Failed");
				}
			}
			out.println("</body>");
			out.println("</html>");
		
	}

}