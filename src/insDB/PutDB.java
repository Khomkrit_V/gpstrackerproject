package insDB;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mongodb.*;


public class PutDB extends getdat.getDatabase {
	public static boolean ins_device(String imei,String company_id){

		boolean flag=false;
		DBCollection coll;
		BasicDBObject query;
		Mongo m;
		DB db;
		int row =0;
		try{
			m = getMongoConnection();
			db = m.getDB( "gts" );
			coll = db.getCollection("device");
			int rowcount = getRowCount("device")+1;
			String device_id="DV0"+rowcount;
			boolean ch =checkDupdata("device","Device_ID",device_id);
			if(ch){
				rowcount=0;
			}
			while(ch){
				rowcount=rowcount+1;
				device_id="DV0"+rowcount;
				ch =checkDupdata("device","Device_ID",device_id);
			}
			query = new BasicDBObject();
			query.put("Device_ID", device_id);
			query.put("Vehicle_ID", "");
			query.put("Company_ID", company_id);
			query.put("imei", imei);
			query.put("stat", "0");
			coll.insert(query);
			flag=true;
			m.close();		
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return flag;
	}
	public static boolean ins_driver(String company_id,String first_name1,String last_name1,String province1,String city1,String address1,String phone1,String role,String year1,String month1,String day1,String sex1,String email) throws UnsupportedEncodingException{
		boolean flag=false;
		DBCollection coll;
		BasicDBObject query;
		Mongo m;
		DB db;
		String first_name =new String(first_name1.getBytes("UTF-8"), "UTF8");
		String last_name =new String(last_name1.getBytes("UTF-8"), "UTF8");
		String province =new String(province1.getBytes("UTF-8"), "UTF8");
		String city =new String(city1.getBytes("UTF-8"), "UTF8");
		String address =new String(address1.getBytes("UTF-8"), "UTF8");
		String phone =new String(phone1.getBytes("UTF-8"), "UTF8");
		String year =new String(year1.getBytes("UTF-8"), "UTF8");
		String month =new String(month1.getBytes("UTF-8"), "UTF8");
		String day =new String(day1.getBytes("UTF-8"), "UTF8");
		String sex =new String(sex1.getBytes("UTF-8"), "UTF8");
		int row =0;
			try{
				m = getMongoConnection();
				db = m.getDB( "gts" );
				coll = db.getCollection("driver");
				int rowcount = getRowCount("driver")+1;
				String driver_id="D0"+rowcount;
				boolean ch =checkDupdata("driver","Driver_ID",driver_id);
				if(ch){
					rowcount=0;
				}
				while(ch){
					rowcount=rowcount+1;
					driver_id="D0"+rowcount;
					ch =checkDupdata("driver","Driver_ID",driver_id);
				}
				query = new BasicDBObject();
				query.put("Driver_ID", driver_id);
				query.put("Company_ID", company_id);
				query.put("First_name", first_name);
				query.put("Last_name", last_name);
				query.put("Year", year);
				query.put("Month", month);
				query.put("Day", day);
				query.put("Sex", sex);
				query.put("Province", province);
				query.put("City", city);
				query.put("Address", address);
				query.put("E-Mail", email);
				query.put("Phone", phone);
				query.put("stat", "0");
				coll.insert(query);
				flag=true;
				m.close();
				
			}catch(Exception e){
				e.printStackTrace();
			}
		
		return flag;
	}
	public static boolean ins_Customer(String company_id,String cus_name1,String country1,String province1,String city1,String address1,String phone1,String year1,String month1,String day1 ,String sex1,String email){
		boolean flag=false;
		DBCollection coll;
		BasicDBObject query;
		Mongo m;
		DB db;		
		int row =0;
			try{
				String phone =new String(phone1.getBytes("UTF-8"), "UTF8");
				String cus_name =new String(cus_name1.getBytes("UTF-8"), "UTF8");
				String country =new String(country1.getBytes("UTF-8"), "UTF8");
				String province =new String(province1.getBytes("UTF-8"), "UTF8");
				String city =new String(city1.getBytes("UTF-8"), "UTF8");
				String address =new String(address1.getBytes("UTF-8"), "UTF8");
				String year =new String(year1.getBytes("UTF-8"), "UTF8");
				String month =new String(month1.getBytes("UTF-8"), "UTF8");
				String day =new String(day1.getBytes("UTF-8"), "UTF8");
				String sex =new String(sex1.getBytes("UTF-8"), "UTF8");
				m = getMongoConnection();
				db = m.getDB( "gts" );
				coll = db.getCollection("customer");
				int rowcount = getRowCount("customer")+1;
				String customer_id="CUS0"+rowcount;
				boolean ch =checkDupdata("customer","Customer_ID",customer_id);
				if(ch){
					rowcount=0;
				}
				while(ch){
					rowcount=rowcount+1;
					customer_id="CUS0"+rowcount;
					ch =checkDupdata("customer","Customer_ID",customer_id);
				}
				query = new BasicDBObject();
				query.put("Customer_ID", customer_id);
				query.put("Company_ID", company_id);
				query.put("Customer_name", cus_name);
				query.put("Year", year);
				query.put("Month", month);
				query.put("Day", day);
				query.put("Sex", sex);
				query.put("Country", country);
				query.put("Province", province);
				query.put("City", city);
				query.put("Address", address);
				query.put("Phone", phone);
				query.put("E-Mail", email);
				query.put("stat", "0");
				coll.insert(query);
				flag=true;
				m.close();
				System.gc();
				
			}catch(Exception e){
				e.printStackTrace();
			}
		
		return flag;
	}
	public static boolean ins_vehicle(String device_id,String registerID1,String company_id,String type1,String user_stat,String year1,String month1,String day1,String gear1,String brand1,String model1,String color1,String description1,String engine1){
		boolean flag=false;
		DBCollection coll;
		DBCollection coll2;
		BasicDBObject query=new BasicDBObject() ;
		BasicDBObject query2=new BasicDBObject() ;
		Mongo m;
		DB db;
		int row =0;
			try{
				String registerID =new String(registerID1.getBytes("UTF-8"), "UTF8");
				String type =new String(type1.getBytes("UTF-8"), "UTF8");
				String year =new String(year1.getBytes("UTF-8"), "UTF8");
				String month =new String(month1.getBytes("UTF-8"), "UTF8");
				String day =new String(day1.getBytes("UTF-8"), "UTF8");
				String gear =new String(gear1.getBytes("UTF-8"), "UTF8");
				String brand =new String(brand1.getBytes("UTF-8"), "UTF8");
				String model =new String(model1.getBytes("UTF-8"), "UTF8");
				String color =new String(color1.getBytes("UTF-8"), "UTF8");
				String description =new String(description1.getBytes("UTF-8"), "UTF8");
				String engine =new String(engine1.getBytes("UTF-8"), "UTF8");
				
				m = getMongoConnection();
				db = m.getDB( "gts" );
				coll = db.getCollection("device");
				int rowcount = getRowCount("vehicle")+1;
				String vehicle_id="VE0"+rowcount;
				boolean ch =checkDupdata("vehicle","Vehicle_ID",vehicle_id);
				if(ch){
					rowcount=0;
				}
				while(ch){
					rowcount=rowcount+1;
					vehicle_id="VE0"+rowcount;
					ch =checkDupdata("vehicle","Vehicle_ID",vehicle_id);
				}
				System.out.println("vehicle id is    "+vehicle_id);
				query.put("Device_ID", device_id);
				DBCursor cur1=coll.find(query);
				boolean isrs2null=cur1.hasNext();
				String dstat="";
				if(!isrs2null){
					System.out.println("don't find device with that ID");
				}else{
					
					dstat=(String) cur1.next().get("stat");
				}
				coll2=db.getCollection("vehicle");
				if(device_id.equals("null")){
					query2.put("Vehicle_ID", vehicle_id);
					query2.put("Device_ID", "");
					query2.put("RegisterID", registerID);
					query2.put("Company_ID", company_id);
					query2.put("Year", year);
					query2.put("Month", month);
					query2.put("Day", day);
					query2.put("Gear", gear);
					query2.put("Brand", brand);
					query2.put("Model", model);
					query2.put("Color", color);
					query2.put("Description", description);
					query2.put("Engine", engine);
					query2.put("Type", type);
					query2.put("stat", "0");
					coll2.insert(query2);
					flag=true;
					
				}else if(dstat.equals("1")){
					flag=false;
				}
				else{
					BasicDBObject query3=new BasicDBObject() ;
					query3.put("Vehicle_ID", vehicle_id);
					query3.put("Device_ID", device_id);
					query3.put("RegisterID", registerID);
					query3.put("Company_ID", company_id);
					query3.put("Year", year);
					query3.put("Month", month);
					query3.put("Day", day);
					query3.put("Gear", gear);
					query3.put("Brand", brand);
					query3.put("Model", model);
					query3.put("Color", color);
					query3.put("Description", description);
					query3.put("Engine", engine);
					query3.put("Type", type);
					query3.put("stat", "0");
					coll2.insert(query3);
					BasicDBObject query4=new BasicDBObject() ;
					BasicDBObject query5=new BasicDBObject() ;
					BasicDBObject query6=new BasicDBObject() ;
					query4.put("Device_ID", device_id);
					query5.put("Vehicle_ID", vehicle_id);
					query5.put("stat", "1");
					query6.put("$set", query5);
					coll.update(query4, query6);
					//st.executeUpdate("UPDATE device SET stat = '0',Vehicle_ID='"+vehicle_id+"' WHERE Device_ID = '"+device_id+"'");
					flag=true;
				}
				m.close();
			}catch(Exception e){
				e.printStackTrace();
			}
		return flag;
		
	}
	public static String ins_deliver(String vehicle_id,String driver_id,String company_id,String destination1,String origin1){
		boolean flag=false;
		DBCollection coll;
		DBCollection coll2;
		DBCollection coll3;
		BasicDBObject query0=new BasicDBObject() ;
		BasicDBObject query=new BasicDBObject() ;
		BasicDBObject query2=new BasicDBObject() ;
		Mongo m;
		DB db;
		String deliver_id="";
		int row =0;
			try{
				String destination =new String(destination1.getBytes("UTF-8"), "UTF8");
				String origin =new String(origin1.getBytes("UTF-8"), "UTF8");
				m = getMongoConnection();
				db = m.getDB( "gts" );
				coll = db.getCollection("deliver");
				int rowcount = getRowCount("deliver")+1;
				deliver_id="DEL0"+rowcount;
				boolean ch =checkDupdata("deliver","Deliver_ID",deliver_id);
				if(ch){
					rowcount=0;
				}
				while(ch){
					rowcount=rowcount+1;
					deliver_id="DEL0"+rowcount;
					ch =checkDupdata("deliver","Deliver_ID",deliver_id);
				}
				System.out.println("Deliver id is    "+deliver_id);
				query.put("Deliver_ID", deliver_id);
				query.put("Vehicle_ID", vehicle_id);
				query.put("Driver_ID", driver_id);
				query.put("Company_ID", company_id);
				query.put("Destination", destination);
				query.put("Origin", origin);
				query.put("stat", "1");
				coll3=db.getCollection("vehicle");
				query0.put("Vehicle_ID", vehicle_id);
				DBCursor cur=coll3.find(query0);
				String statVeh="5";
				if(cur.hasNext()){
					statVeh = (String) cur.next().get("stat");
				}else{
					System.out.println("no vehicle data in ins VEHICLE");
				}
				query0.clear();
				coll3=db.getCollection("driver");
				query0.put("Driver_ID", driver_id);
				cur=coll3.find(query0);
				String statDri="5";
				if(cur.hasNext()){
					statDri = (String) cur.next().get("stat");
				}else{
					System.out.println("no driver data in ins VEHICLE");
				}
				if(statVeh.equals("0")&&statDri.equals("0")){
					coll.insert(query);//���ͺ�
				
					BasicDBObject query3=new BasicDBObject() ;
					BasicDBObject query4=new BasicDBObject() ;
					BasicDBObject query5=new BasicDBObject() ;
				
					query2.put("stat", "1");
					query3.put("Vehicle_ID", vehicle_id);
					query4.put("Driver_ID", driver_id);
					query5.put("$set", query2);
					coll2=db.getCollection("driver");
					coll2.update(query4, query5);
					coll2=db.getCollection("vehicle");
					coll2.update(query3, query5);
					flag=true;
				}else{
					deliver_id="";
					flag=false;
				}
				m.close();
				
			}catch(Exception e){
				e.printStackTrace();
				
			}
		return deliver_id;
		
	}
	public static boolean ins_product(String company_id,String deliver_id,String product_id,String product_name1,String description1,String price,String amount,String customer){
		boolean flag=false;
		DBCollection coll;		
		BasicDBObject query=new BasicDBObject() ;		
		Mongo m;
		DB db;		
			try{
				String product_name =new String(product_name1.getBytes("UTF-8"), "UTF8");
				String description =new String(description1.getBytes("UTF-8"), "UTF8");
				m = getMongoConnection();
				db = m.getDB( "gts" );
				coll = db.getCollection("product");
				query.put("Company_ID", company_id);
				query.put("Deliver_ID", deliver_id);
				query.put("Product_ID", product_id);
				query.put("Product_Name", product_name);
				query.put("Description", description);				
				query.put("Price", price);
				query.put("Amount", amount);
				query.put("Customer_ID", customer);				
				coll.insert(query);
				m.close();
				flag=true;
				
			}catch(Exception e){
				e.printStackTrace();
				
			}
		return flag;
		
	}
	public static boolean update_data(String collection,String fild,String dat_tar,BasicDBObject dat_up){
		boolean flag=false;
		DBCollection coll;	
		BasicDBObject query=new BasicDBObject() ;
		BasicDBObject query2=new BasicDBObject() ;
		Mongo m;
		DB db;
		
			try{
				m = getMongoConnection();
				db = m.getDB( "gts" );
				coll = db.getCollection(collection);
				query.put(fild, dat_tar);
				query2.put("$set", dat_up);
				coll.update(query, query2);				
				flag=true;
				m.close();			
			}catch(Exception e){
				flag=false;
				e.printStackTrace();
				
			}
			
		return flag;
		
	}
	public static boolean update_data(String collection,BasicDBObject dat_tar,BasicDBObject dat_up){
		boolean flag=false;
		DBCollection coll;			
		BasicDBObject query2=new BasicDBObject() ;
		Mongo m;
		DB db;
		
			try{
				m = getMongoConnection();
				db = m.getDB( "gts" );
				coll = db.getCollection(collection);				
				query2.put("$set", dat_up);
				coll.update(dat_tar, query2);				
				flag=true;
				m.close();			
			}catch(Exception e){
				flag=false;
				e.printStackTrace();
				
			}
			
		return flag;
		
	}
	public static boolean insCollData(String collection,BasicDBObject query){
		boolean flag=false;
		DBCollection coll;			
		Mongo m;
		DB db;
		
			try{
				m = getMongoConnection();
				db = m.getDB( "gts" );
				coll = db.getCollection(collection);
				coll.insert(query);			
				flag=true;
				m.close();			
			}catch(Exception e){
				flag=false;
				e.printStackTrace();
				
			}
			
		return flag;
		
	}
	public static boolean ins_Material(String company_id,String manu_id,String id_part,String part_name,String part_des,String part_amount,String part_W,String part_V,String part_price,String seller){
		boolean flag=false;
		DBCollection coll;			
		Mongo m;
		DB db;
		
			try{
				m = getMongoConnection();
				db = m.getDB( "gts" );
				coll = db.getCollection("material");
				BasicDBObject query=new BasicDBObject();
				query.put("Company_ID", company_id);
				query.put("Manufacture_ID",""+new String(manu_id.getBytes("ISO-8859-1"), "UTF8"));
				query.put("Part_ID", ""+new String(id_part.getBytes("ISO-8859-1"), "UTF8"));
				query.put("Part_Name",""+new String(part_name.getBytes("ISO-8859-1"), "UTF8") );
				query.put("Part_description",""+new String(part_des.getBytes("ISO-8859-1"), "UTF8") );
				query.put("Part_Amount",""+new String(part_amount.getBytes("ISO-8859-1"), "UTF8") );
				query.put("Part_Weight",""+new String(part_W.getBytes("ISO-8859-1"), "UTF8") );
				query.put("Part_volume",""+new String(part_V.getBytes("ISO-8859-1"), "UTF8") );
				query.put("Part_Price",""+new String(part_price.getBytes("ISO-8859-1"), "UTF8") );
				query.put("Part_Seller",""+new String(seller.getBytes("ISO-8859-1"), "UTF8") );
				query.put("Part_Stat", "0");
				coll.insert(query);
				m.close();
				flag=true;
			}catch(Exception e){
				flag=false;
				e.printStackTrace();
			}
		return flag;
	}
	public static boolean ins_manufacture(String company_id,String manu_id,String manu_name,String manu_amount,String order_day,String order_month,String order_year,String in_day,String in_month,String in_year,String warehouse_id){
		boolean flag=false;
		DBCollection coll;			
		Mongo m;
		DB db;
		
			try{
				String manu2_id=new String(manu_id.getBytes("ISO-8859-1"), "UTF8");
				m = getMongoConnection();
				db = m.getDB( "gts" );
				coll = db.getCollection("manufacture");
				BasicDBObject query=new BasicDBObject();
				query.put("Company_ID",""+new String(company_id.getBytes("ISO-8859-1"), "UTF8") );
				query.put("Manufacture_ID",manu2_id );
				query.put("Manufacture_Name",""+new String(manu_name.getBytes("ISO-8859-1"), "UTF8") );
				query.put("Manufacture_Amount",""+new String(manu_amount.getBytes("ISO-8859-1"), "UTF8") );
				query.put("order_day",""+new String(order_day.getBytes("ISO-8859-1"), "UTF8") );
				query.put("order_month",""+new String(order_month.getBytes("ISO-8859-1"), "UTF8") );
				query.put("order_year",""+new String(order_year.getBytes("ISO-8859-1"), "UTF8") );
				query.put("in_day",""+new String(in_day.getBytes("ISO-8859-1"), "UTF8") );
				query.put("in_month",""+new String(in_month.getBytes("ISO-8859-1"), "UTF8") );
				query.put("in_year",""+new String(in_year.getBytes("ISO-8859-1"), "UTF8") );
				query.put("Build_Approve", "0");
				query.put("Warehouse_ID", ""+new String(warehouse_id.getBytes("ISO-8859-1"), "UTF8") );
				query.put("Build_Stat", "0");
				query.put("Part_Stat", "0");
				coll.insert(query);
				m.close();
				flag=true;
			}catch(Exception e){
				flag=false;
				e.printStackTrace();
			}
		return flag;
	}
	public static boolean ins_vendor(String company_id,String v_name,String v_con_name,String v_phone,String v_fax,
			String v_email,String v_website,String v_address,String v_pay_method,String v_tran,
			String v_currency,String v_tax,String v_remark){
		boolean flag=false;
		DBCollection coll;			
		Mongo m;
		DB db;
		
			try{
				String manu2_id=new String(v_name.getBytes("ISO-8859-1"), "UTF8");
				m = getMongoConnection();
				db = m.getDB( "gts" );
				coll = db.getCollection("vendor");
				BasicDBObject query=new BasicDBObject();
				BasicDBObject query2=new BasicDBObject();
				query2.put("Company_ID",""+new String(company_id.getBytes("ISO-8859-1"), "UTF8") );
				query2.put("Vendor_Name",manu2_id );
				DBCursor cur = coll.find(query2);				
				query.put("Company_ID",""+new String(company_id.getBytes("ISO-8859-1"), "UTF8") );
				query.put("Vendor_Name",manu2_id );
				query.put("Vendor_Con_Name",""+new String(v_con_name.getBytes("ISO-8859-1"), "UTF8")+" " );
				query.put("Vendor_Phone",""+new String(v_phone.getBytes("ISO-8859-1"), "UTF8")+" " );
				query.put("Vendor_Fax",""+new String(v_fax.getBytes("ISO-8859-1"), "UTF8")+" " );
				query.put("Vendor_Email",""+new String(v_email.getBytes("ISO-8859-1"), "UTF8")+" " );
				query.put("Vendor_Website",""+new String(v_website.getBytes("ISO-8859-1"), "UTF8")+" " );
				query.put("Vendor_Address",""+new String(v_address.getBytes("ISO-8859-1"), "UTF8")+" " );
				query.put("Vendor_Pay",""+new String(v_pay_method.getBytes("ISO-8859-1"), "UTF8")+" " );
				query.put("Vendor_Tran",""+new String(v_tran.getBytes("ISO-8859-1"), "UTF8")+" " );
				query.put("Vendor_Currency", ""+new String(v_currency.getBytes("ISO-8859-1"), "UTF8"));
				query.put("Vendor_Remark", ""+new String(v_remark.getBytes("ISO-8859-1"), "UTF8")+" " );
				query.put("Vendor_Tax", ""+new String(v_tax.getBytes("ISO-8859-1"), "UTF8")+" ");				
				if(cur.hasNext()){
					update_data("vendor", query2, query);
				}else{
					coll.insert(query);
				}
				m.close();
				flag=true;
			}catch(Exception e){
				flag=false;
				e.printStackTrace();
			}
		return flag;
	}
	
}

