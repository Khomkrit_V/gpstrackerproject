package editRead;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mongodb.DBObject;

public class R_device extends HttpServlet {
	static String usn="";
	static String pwd ="";
	static String user_stat="";
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp, false);
	}
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp, true);
	}
	public static void doWork(HttpServletRequest req, HttpServletResponse resp,boolean isPost)throws ServletException, IOException {
		
		HttpSession ses = req.getSession();
		ses.setAttribute("wr1", "Session out");
		usn =(String) ses.getAttribute("usn");
		pwd =(String) ses.getAttribute("pwd");
		user_stat=getdat.getDatabase.getUserdata(usn, pwd);
		String company_id = (String) ses.getAttribute("company_id");
		ses.setAttribute("task", "device");
		if(user_stat.equals("")){
			
			String wr="Wrong ID or Password";
			req.setAttribute("wrong", wr);
			RequestDispatcher view = req.getRequestDispatcher("start.jsp?p1=123456");
			view.forward(req, resp);
						    
		}
		//System.out.println(usn+"\n"+pwd+"\n"+user_stat);
		RequestDispatcher view = req.getRequestDispatcher("readPage.jsp");
		view.forward(req, resp);
		int row = 0;
		row = getdat.getDatabase.getRowCount("device", "Company_ID", company_id);
		DBObject[] deviceDat =null;
		deviceDat=getdat.getDatabase.getDeviceCompany(company_id, row);
		String[] device_id =new String[row+1];
		String[] vehicle_id =new String[row+1];
		String[] imei=new String[row+1];
		String[] stat=new String[row+1];
		for(int i=1;i<=row;i++){
			device_id[i]=(String) deviceDat[i].get("Device_ID");
			vehicle_id[i]=(String) deviceDat[i].get("Vehicle_ID");
			if(vehicle_id[i].equalsIgnoreCase("")){
				vehicle_id[i]="don't add to any vehicle";
			}
			imei[i]=(String) deviceDat[i].get("imei");
			stat[i]=(String) deviceDat[i].get("stat");
		}
		resp.setContentType("text/html");
		PrintWriter out =resp.getWriter();
		// web code
		out.println(" <!DOCTYPE html>"); 
		out.println("<html>"); 
		out.println("<head>");
		
		out.println("</head>");

		out.println("<body >");
		
		out.println("<table border=\"1\">");
		out.println("<caption>Device Detail</caption>");
		out.println("<tr><th>Device ID</th><th>Vehicle ID</th><th>IMEI ID</th><th>stat</th></tr>");
		for(int i=1;i<=row;i++){
			out.println("<tr><td>"+device_id[i]+"</td><td>"+vehicle_id[i]+"</td><td>"+imei[i]+ "</td><td>"+stat[i]+"</td>");
			out.println("<td>");
			out.println("<form name=\"formmy"+i+"\"action=\"E_device\" method=\"post\">");
			//out.println("<input type=\"checkbox\" id=\"dv\" checked=\"checked\" name=\"dv\" value=\"testttttt\" /> ");
			out.println("<input type=\"button\" value=\"Edit\" onclick=\"window.location.href='./E_edit?dv="+device_id[i]+"&dd=0&task=device'\"/>");
			out.println("<input type=\"button\" value=\"Delete\" onclick=\"window.location.href='./E_edit?dv="+device_id[i]+"&dd=yes&task=device'\"/>");
			out.println("</form>");
			out.println("</td>");
			out.println("</tr>");
		}
		out.println("</table>");
		out.println("</body>");
		
		out.println("</html>");
		
	}
	

}
