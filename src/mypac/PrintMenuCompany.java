package mypac;

import java.io.IOException;
import com.mongodb.*;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class PrintMenuCompany extends HttpServlet {
	static String usn="";
	static String pwd ="";
	static String user_stat="";
	static String root_url="";
	

	public static void doWork(HttpServletRequest req, HttpServletResponse resp,boolean isPost,String username,String password)
			throws ServletException, IOException {
		HttpSession ses = req.getSession();
		ses.setAttribute("wr1", "Session out");
		DBObject companyData=null;
		DBObject[] vehicleData = null;
		usn=req.getParameter("usn");
		pwd=req.getParameter("pwd");
		user_stat=getdat.getDatabase.getUserdata(usn, pwd);
		int rowcount=0;
		String company_name= "";
		ses.setAttribute("usn", usn);
		ses.setAttribute("pwd", pwd);
		ses.setAttribute("user_stat", user_stat);
		companyData=getdat.getDatabase.getCompanyData(usn);
		company_name=(String) companyData.get("Company_Name");
		String company_id=(String) companyData.get("Company_ID");
		rowcount=getdat.getDatabase.getVehicleCompanyRow(company_id);
		vehicleData=getdat.getDatabase.getVehicleCompany(company_id, rowcount);
		String[] vehicle_id=new String[rowcount+1];
		
		if(vehicleData!=null){
			for(int i=1;i<=rowcount;i++){
				vehicle_id[i]=(String) vehicleData[i].get("Vehicle_ID");
			}
		}else{
			System.out.println("no vehicle Data");
		}
		/*///////////////////////////// not complete
		String deliver_id = getdat.getDatabase.getActiveDeliverID(vehicle_id[2]);
		String driver_id,driver_name;
		if(deliver_id.equalsIgnoreCase("notactive")){
			driver_id="notactive";
			driver_name="notactive";
		}else{
			driver_id = (String) getdat.getDatabase.getCollData("deliver", "Deliver_ID", deliver_id).get("Driver_ID");
			driver_name = (String) getdat.getDatabase.getCollData("driver", "Driver_ID", driver_id).get("First_name")+"  "+(String) getdat.getDatabase.getCollData("driver", "Driver_ID", driver_id).get("Last_name");
		}
		////////////////////////////// not complete*/
		
		if(user_stat.equals("")){
			
			String wr="Wrong ID or Password";
			req.setAttribute("wrong", wr);
			RequestDispatcher view = req.getRequestDispatcher("start.jsp?note="+wr+"");
			view.forward(req, resp);
						    
		}else{
			RequestDispatcher view = req.getRequestDispatcher("Menu_Track.jsp");
			view.forward(req, resp);
			/*if(user_stat.equals("company")){
				ses.setAttribute("company_id", company_id);
			}
		// web code html
			resp.setContentType("text/html");
			PrintWriter out =resp.getWriter();
			
			out.println(" <!DOCTYPE html>"); 
			out.println("<html>"); 
			out.println("<head>");
			out.println("<script type=\"text/javascript\"");
			out.println("src=\"./js/util.js\">");
			out.println("</script>");
			out.println("<script type=\"text/javascript\"");
			out.println("src=\"./js/Gmap.js\">");
			out.println("</script>");
			out.println("</head>");

			out.println("<body onload=\"initMenu()\">");
			
			out.println("<div id=\"container\" style=\"height:100%;width:100%\">");
			
			out.println("<div id=\"header\" >");
			if(user_stat.equals("company")){
				out.println("<h1 style=\"margin-bottom:0;\" align=\"center\">Menu page for "+company_name+"<br /></h1></div>");
			}else{
				out.println("<h1 style=\"margin-bottom:0;\" align=\"center\">Vehicle Tracking page for Unknow</h1> < br/></div>");
			}
			out.println("<div id=\"Detail\" style=\"height:80%;width:15%;float:left;\">");
			out.println("<form name=\"vehicle_d\" method=\"post\" action=\"\" target= \"iframe_a\">");
			out.println("Vehicle ID <br />"); 
			//out.println("rowcount :: "+rowcount+"<br />");
			out.println("<select id=\"vehicle_ID\" name=\"Vehicle_ID\" onchange=\"veh_change()\">");
			for(int i=1;i<=rowcount;i++){
			out.println("<option value=\""+vehicle_id[i]+"\">"+vehicle_id[i]+"</option>");
			
			}
			ses.setAttribute("company_name", company_name);
			out.println("</select>");
			out.println("<br />");
			
			out.println("</form>");
			out.println("<input type=\"button\" value=\"view detail\" onclick=\"send_report(1)\"/>");
			out.println("<input type=\"button\" value=\"View Report\" onclick=\"send_report(0)\"/>");
			out.println("<br />Vehicle ID : <label id=\"veh_id\"></label><br />"); 
			out.println("Detail<br />");
			out.println("Car registerID : <label id=\"veh_regid\"></label><br />");
			out.println("Driver Name : <label id=\"driver_name\"></label><br />");
			out.println("Deliver ID : <label id=\"del_id\"></label><br />");
			/*if(deliver_id.equalsIgnoreCase("notactive")){
				out.println("this vehicle is not deliver any thing <br />");
			}*/
			/*out.println("<a href=\"Product_Detail\" target=\"_blank\">Product Detail</a><br/>");
			out.println("</div>");
			
			out.println("<div id=\"map_canvas\" style=\"height:80%;width:70%;float:left;\">");
			out.println("<iframe src=\"\" name=\"iframe_a\"style=\"height:100%;width:100%;\"></iframe>");
			out.println("</div>");
			
			out.println("<div id=\"Detail\" style=\"height:80%;width:15%;float:right;\">");
			// Vehicle
			out.println("Vehicle <br />");
			out.println("<form name=\"addvehicle\" method=\"post\" action=\"addvehicle1\" target= \"_blank\">");
			out.println("<input type=\"submit\" value=\"Add\" />");
			out.println("</form>");
			out.println("<form name=\"r_vehicle\" method=\"post\" action=\"rVehicle\" target= \"_blank\">");
			out.println("<input type=\"submit\" value=\"Read & Edit & Delete\" />");
			out.println("</form>");
			// Device
			out.println("Device<br />");
			out.println("<form name=\"adddivice\" method=\"post\" action=\"adddevice1\" target= \"_blank\">");
			out.println("<input type=\"submit\" value=\"Add\" />");
			out.println("</form>");
			out.println("<form name=\"r_device\" method=\"post\" action=\"rDevice\" target= \"_blank\">");
			out.println("<input type=\"submit\" value=\"Read & Edit & Delete\" />");
			out.println("</form>");
			//Driver
			out.println("Driver<br />");
			out.println("<form name=\"adddriver\" method=\"post\" action=\"adddriver1\" target= \"_blank\">");
			out.println("<input type=\"submit\" value=\"Add\" />");
			out.println("</form>");
			out.println("<form name=\"r_driver\" method=\"post\" action=\"rDriver\" target= \"_blank\">");
			out.println("<input type=\"submit\" value=\"Read & Edit & Delete\" />");
			out.println("</form>");
			//deliver
			out.println("deliver<br />");
			out.println("<form name=\"adddeliver\" method=\"post\" action=\"adddeliver1\" target= \"_blank\">");
			out.println("<input type=\"submit\" value=\"Add\" />");
			out.println("</form>");
			out.println("<form name=\"r_deliver\" method=\"post\" action=\"rDeliver\" target= \"_blank\">");
			out.println("<input type=\"submit\" value=\"Read & Edit & Delete\" />");
			out.println("</form>");
			
			out.println("</div>");
			
			out.println("<div id=\"footer\" style=\"clear:both;text-align:center;\">");
			out.println("KMITL Com Science Radio. <br />");
			out.println("<embed src=\"http://player.thairadioservice.com/flashplayer.swf\" width=\"331\" height=\"25\" allowscriptaccess=\"always\" flashvars=\"file=http://161.246.60.65:80/;stream.nsv&amp;type=mp3&amp;stretch=none&amp;autostart=true&amp;displayclick=none&amp;bgcolor=#FFFFFF&amp;frontcolor=#000000&amp;backcolor=#FFFFFF&amp;lightcolor=#ff9900&amp;screencolor=#000000\">");
			out.println("</div>");
			
			out.println("role is :"+user_stat+" <br/>");
			out.println("</div>");
			out.println("</body>");
			
			out.println("</html>");*/
		}
		System.gc();
	}
	public static void doWork0(HttpServletRequest req, HttpServletResponse resp,boolean isPost,String username,String password)
			throws ServletException, IOException {
		HttpSession ses = req.getSession();
		ses.setAttribute("wr1", "Session out");
		DBObject companyData=null;		
		usn=req.getParameter("usn");
		pwd=req.getParameter("pwd");
		user_stat=getdat.getDatabase.getUserdata(usn, pwd);
		root_url=req.getParameter("root_url");
		//System.out.println(root_url);
		String company_name= "";
		ses.setAttribute("usn", usn);
		ses.setAttribute("pwd", pwd);
		ses.setAttribute("root_url", root_url);
		ses.setAttribute("user_stat", user_stat);
		companyData=getdat.getDatabase.getCompanyData(usn);
		company_name=(String) companyData.get("Company_Name");
		String company_id=(String) companyData.get("Company_ID");			
		if(user_stat.equals("")){
			
			String wr="Wrong ID or Password";
			req.setAttribute("wrong", wr);
			RequestDispatcher view = req.getRequestDispatcher("start.jsp?note="+wr+"");
			view.forward(req, resp);
						    
		}else{
			RequestDispatcher view = req.getRequestDispatcher("Menu_Main.jsp");
			view.forward(req, resp);
			ses.setAttribute("company_id", company_id);
		}
		System.gc();
	}
}
