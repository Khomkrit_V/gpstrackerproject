package mypac;
import java.io.*;
import java.util.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mongodb.DBObject;

public class Track_company extends HttpServlet {
	static String user_stat="company";
	static String usn="";
	static String pwd="";
	
	public static void doWork(HttpServletRequest req, HttpServletResponse resp,boolean isPost,String username,String password)
			throws ServletException, IOException {
		HttpSession ses = req.getSession();
		ses.setAttribute("wr1", "Session out");
		String company_name="";
		DBObject[] vehicleData =new DBObject[2];
		usn=username;
		pwd=password;
		user_stat=getdat.getDatabase.getUserdata(usn, pwd);
		String vehicle_id =req.getParameter("Vehicle_ID");
		company_name = (String) ses.getAttribute("company_name");		
		int rowcount=getdat.getDatabase.getRowCount("eventdata", "Vehicle_ID", vehicle_id);
		String deliver_id = getdat.getDatabase.getActiveDeliverID(vehicle_id);
		double[] lat=null;
		double[] lon=null;
		if(rowcount>0){
			lat= new double[rowcount+1];
			lon= new double[rowcount+1];
			getdat.getDatabase.geteventdata(lat, lon, deliver_id, vehicle_id);
			
		}
		// web code html
			resp.setContentType("text/html");
			PrintWriter out =resp.getWriter();
		
			out.println(" <!DOCTYPE html>"); 
			out.println("<html>"); 
			out.println("<head>");
			out.println("<script type=\"text/javascript\"");
			out.println("src=\"./js/infowin.js\">");
			out.println("</script>");
			out.println("<script type=\"text/javascript\"");
			out.println("src=\"./js/Gmap.js\">");
			out.println("<script type=\"text/javascript\"");
			out.println("src=\"./js/util.js\">");
			out.println("</script>");
			out.println("<script type=\"text/javascript\"");
			out.println("src=\"http://maps.googleapis.com/maps/api/js?key=AIzaSyAT47leMWFTUMX6SiYeGfGMDJoZqa4BwZM&sensor=false\">");
			out.println("</script>");			
			out.println("</head>");
			out.println("<body onload=\"initialize_GMAP('"+vehicle_id+"')\">");
			out.println("<div id=\"container\" style=\"height:100%;width:100%\">");			

			out.println("<div id=\"map_canvas\" style=\"height:600px;width:80%;float:right;\">");
			out.println("</div>");
			
			DBObject delidata =getdat.getDatabase.getCollData("deliver", "Deliver_ID", deliver_id);
			if(delidata!=null){
			String destination=(String) delidata.get("Destination");
			String origin =(String) delidata.get("Origin");
			out.println("<div id=\"footer\" style=\"float:left;height:600px;width:20%;\">");
			out.println("Origin: "+origin+"<br/>");
			out.println("Destination: "+destination+"<br/>");
			out.println("<label id=\"row_new\"></label>_____________<label id=\"row_old\"></label><br />");
			out.println("<label id=\"la1\"></label><br />");
			out.println("<label id=\"la2\"></label><br />");
			out.println("<label id=\"la3\"></label><br />");
			out.println("<label id=\"la4\"></label><br />");
			out.println("<label id=\"la5\"></label><br />");
			out.println("<label id=\"la6\"></label><br />");
			out.println("<label id=\"la7\"></label><br />");
			out.println("<label id=\"la8\"></label><br />");
			out.println("<label id=\"la9\"></label><br />");
			out.println("<label id=\"la10\"></label><br />");
			out.println("<label id=\"la11\"></label><br />");
			out.println("<label id=\"la12\"></label><br />");
			out.println("<label id=\"la13\"></label><br />");
			out.println("</div>");
			out.println("</div>");
			
			}else{
				out.println("not deliver any thing");
			}
			out.println("</body>");
			out.println("</html>");
			System.gc();
		
		
	}

}