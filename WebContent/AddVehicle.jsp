<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%
    String usn,pwd,user_stat;
    HttpSession ses = request.getSession();
	usn=(String) ses.getAttribute("usn");
	pwd=(String) ses.getAttribute("pwd");
	user_stat=(String) ses.getAttribute("user_stat");
	String company_id=(String) ses.getAttribute("company_id");
	int rowcount = getdat.getDatabase.getRowCount("device", "Company_ID", company_id);
	String[] device_id = new String[rowcount+1];
	boolean flag = getdat.getDatabase.getDevice_company(usn, pwd, device_id, company_id,"1");
    %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html><head>
<meta name="XAR Files" content="index_htm_files/xr_files.txt"/>
 <title>Add Vehicle</title>
 <meta http-equiv="Content-Type" content="text/html; charset=Windows-874"/>
 <meta name="Generator" content="Xara HTML filter v.4.0.4.653"/>
 <link rel="stylesheet" type="text/css" href="index_htm_files/xr_main.css"/>
 <link rel="stylesheet" type="text/css" href="index_htm_files/xr_text.css"/>
 <script type="text/javascript" src="index_htm_files/roe.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="HandheldFriendly" content="true" />
<title>Form</title>
<link href="css/FormCss_1.css" rel="stylesheet" type="text/css" />
<link type="text/css" rel="stylesheet" href="css/FormCss_2.css" />
<style type="text/css">
    .form-label{
        width:150px !important;
    }
    .form-label-left{
        width:150px !important;
    }
    .form-line{
        padding-top:12px;
        padding-bottom:12px;
    }
    .form-label-right{
        width:150px !important;
    }
    body, html{
        margin:0;
        padding:0;
        background:false;
    }

    .form-all{
        margin:0px auto;
        padding-top:20px;
        width:650px;
        color:#555 !important;
        font-family:'Lucida Grande';
        font-size:14px;
    }
    .form-radio-item label, .form-checkbox-item label, .form-grading-label, .form-header{
        color:#555;
    }

</style>

<script src="js/FormJS_1.JS" type="text/javascript"></script>
<script type="text/javascript">
   JotForm.init();
</script>
<link href="css/myCustom.css" rel="stylesheet" type="text/css" />
</head>
<body>
<!--[if IE]><div class="xr_ap" id="xr_xri" style="width: 1360px; height: 800px; clip: rect(0px 1360px 800px 0px);"><![endif]-->
<!--[if !IE]>--><div class="xr_ap" id="xr_xri" style="width: 1360px; height: 800px; clip: rect(0px, 1360px, 800px, 0px);"><!--<![endif]-->
 <img class="xr_ap" src="index_htm_files/9.png" alt="" title="" style="left: 1px; top: 4px; width: 1360px; height: 796px;"/>
 
 <label class="nor_text" id="he1" >
          Add Vehicle
        </label>
        <br />
<form class="form_pos2" action="./addvehicle2" method="post" name="form_21" id="21" >
  
  <div class="form-all">
    <ul class="form-section">
      <li class="form-line" id="id_1">
        <label class="form-label-left" id="label_1" for="input_1">
          Register ID<span class="form-required">*</span>
        </label>
        <div id="cid_1" class="form-input">
          <input type="text" class="form-textbox validate[required]" id="input_1" name="car_register_id" size="20" />
        </div>
      </li>
      <li class="form-line" id="id_sex">
        <label class="form-label-left" id="label_sex" for="input_sex">
          Device ID<span class="form-required">*</span>
        </label>
        <select class="form-dropdown validate[required]" style="width:80px" id="input_sex" name="device_id">
            <%for(int i=1;i<=rowcount;i++){
				if(device_id[i]==null){
					
				}else{
					out.println("<option value=\""+device_id[i]+"\">"+device_id[i]+"</option>");
				}
			}
			 %>
          </select>
      </li>
        <li class="form-line" id="id_birthday">
       <label class="form-label-left" id="label_birthday">
          Year of purchase
        </label><br /><br />
                
        <label class="form-label-left1" id="label_birthday_1" for="input_birthday_1">
          Year<span class="form-required">*</span>
        </label>        
          <select class="form-dropdown validate[required]" style="width:70px" id="input_birthday_1" name="year">
            <%
            out.println("<option value=\"\"> </option>");
            for(int i=1900;i<=2500;i++){				
					out.println("<option value=\""+i+"\">"+i+"</option>");					
				
			} %>
          </select>
          <label class="form-label-left1" id="label_birthday_2" for="input_birthday_2">
          Month<span class="form-required">*</span>
        </label>        
          <select class="form-dropdown validate[required]" style="width:50px" id="input_birthday_2" name="month">
            <%
            out.println("<option value=\"\"> </option>");
            for(int i=1;i<=12;i++){		
            	out.println("<option value=\""+i+"\">"+i+"</option>");					
				
			} %>
          </select>
          <label class="form-label-left1" id="label_birthday_3" for="input_birthday_3">
          Day<span class="form-required">*</span>
        </label>        
          <select class="form-dropdown validate[required]" style="width:50px" id="input_birthday_3" name="day">
            <%out.println("<option value=\"\"> </option>");
            for(int i=1;i<=31;i++){
				
            	out.println("<option value=\""+i+"\">"+i+"</option>");
					
				
			} %>
          </select>
        
      </li>
      <li class="form-line" id="id_sex">
        <label class="form-label-left" id="label_sex" for="input_sex">
          Gear<span class="form-required">*</span>
        </label>
        <select class="form-dropdown validate[required]" style="width:80px" id="input_sex" name="gear">
            <%out.println("<option value=\"\"> </option>");		
            	out.println("<option value=\"Auto\">Auto</option>");
            	out.println("<option value=\"Manual\">Manual</option>");
			 %>
          </select>
      </li>
      <li class="form-line" id="id_4">
        <label class="form-label-left" id="label_4" for="input_4">
          Brand<span class="form-required">*</span>
        </label>
        <div id="cid_4" class="form-input">
          <input type="text" class="form-textbox validate[required]" id="input_4" name="brand" size="20" />
        </div>
      </li>
      <li class="form-line" id="id_3">
        <label class="form-label-left" id="label_3" for="input_3">
          Model<span class="form-required">*</span>
        </label>
        <div id="cid_3" class="form-input">
          <input type="text" class="form-textbox validate[required]" id="input_3" name="model" size="20" />
        </div>
      </li>
      <li class="form-line" id="id_5">
        <label class="form-label-left" id="label_5" for="input_5">
          Color<span class="form-required">*</span>
        </label>
        <div id="cid_5" class="form-input">
          <input type="text" class="form-textbox validate[required]" id="input_5" name="color" size="20" />
        </div>
      </li>
      <li class="form-line" id="id_7">
        <label class="form-label-left" id="label_7" for="input_7">
          Description
        </label>
        <div id="cid_7" class="form-input">
          <textarea rows="5" cols="40"   id="input_7" name="description"  ></textarea>
        </div>
      </li>
      <li class="form-line" id="id_email">
        <label class="form-label-left" id="label_email" for="input_email">
          Engine<span class="form-required">*</span>
        </label>
        <div id="cid_email" class="form-input">
          <input type="text" class="form-textbox validate[required]" id="input_email" name="engine" size="20" />
        </div>
      </li>
      <li class="form-line" id="id_6">
        <label class="form-label-left" id="label_6" for="input_6">
          Type<span class="form-required">*</span>
        </label>
        <div id="cid_6" class="form-input">
          <input type="text" class="form-textbox validate[required]" id="input_6" name="type" size="20" />
        </div>
      </li>
      <li class="form-line" id="id_2">
        <div id="cid_2" class="form-input-wide">
          <div style="margin-left:156px" class="form-buttons-wrapper">
            <button id="input_2" type="submit" class="form-submit-button">
              Add
            </button>
          </div>
        </div>
      </li>
      <li style="display:none">
        Should be Empty:
        <input type="text" name="website" value="" />
      </li>
    </ul>
  </div>
  
</form></div></body>

</html>
