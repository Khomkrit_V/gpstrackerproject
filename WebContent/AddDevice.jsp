<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%
    String usn,pwd,user_stat;
    HttpSession ses = request.getSession();
	usn=(String) ses.getAttribute("usn");
	pwd=(String) ses.getAttribute("pwd");
	user_stat=(String) ses.getAttribute("user_stat");
	String company_id=(String) ses.getAttribute("company_id");
    %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html><head>
<meta name="XAR Files" content="index_htm_files/xr_files.txt"/>
 <title>Add Device</title>
 <meta http-equiv="Content-Type" content="text/html; charset=Windows-874"/>
 <meta name="Generator" content="Xara HTML filter v.4.0.4.653"/>
 <link rel="stylesheet" type="text/css" href="index_htm_files/xr_main.css"/>
 <link rel="stylesheet" type="text/css" href="index_htm_files/xr_text.css"/>
 <script type="text/javascript" src="index_htm_files/roe.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="HandheldFriendly" content="true" />
<title>Form</title>
<link href="css/FormCss_1.css" rel="stylesheet" type="text/css" />
<link type="text/css" rel="stylesheet" href="css/FormCss_2.css" />
<style type="text/css">
    .form-label{
        width:150px !important;
    }
    .form-label-left{
        width:150px !important;
    }
    .form-line{
        padding-top:12px;
        padding-bottom:12px;
    }
    .form-label-right{
        width:150px !important;
    }
    body, html{
        margin:0;
        padding:0;
        background:false;
    }

    .form-all{
        margin:0px auto;
        padding-top:20px;
        width:650px;
        color:#555 !important;
        font-family:'Lucida Grande';
        font-size:14px;
    }
    .form-radio-item label, .form-checkbox-item label, .form-grading-label, .form-header{
        color:#555;
    }

</style>

<script src="js/FormJS_1.JS" type="text/javascript"></script>
<script type="text/javascript">
   JotForm.init();
</script>
<link href="css/myCustom.css" rel="stylesheet" type="text/css" />
</head>
<body>
<!--[if IE]><div class="xr_ap" id="xr_xri" style="width: 1360px; height: 800px; clip: rect(0px 1360px 800px 0px);"><![endif]-->
<!--[if !IE]>--><div class="xr_ap" id="xr_xri" style="width: 1360px; height: 800px; clip: rect(0px, 1360px, 800px, 0px);"><!--<![endif]-->
 <img class="xr_ap" src="index_htm_files/9.png" alt="" title="" style="left: 1px; top: 4px; width: 1360px; height: 796px;"/>
 
 <label class="nor_text" id="he1" >
          Add Device
        </label>
        <br />
<form class="form_pos2" action="adddevice2" method="post" name="form_22" id="22" >
  
  <div class="form-all">
    <ul class="form-section">
      <li class="form-line" id="id_imei">
        <label class="form-label-left" id="imei1" for="imei">
          Device Imei<span class="form-required">*</span>
        </label>
        <div id="cid_1" class="form-input">
          <input type="text" class="form-textbox validate[required]" id="imei" name="imei" size="20" />
        </div>
      </li>      
      <li class="form-line" id="id_2">
        <div id="cid_2" class="form-input-wide">
          <div style="margin-left:156px" class="form-buttons-wrapper">
            <button id="input_2" type="submit" class="form-submit-button">
              Add
            </button>
          </div>
        </div>
      </li>
      <li style="display:none">
        Should be Empty:
        <input type="text" name="website" value="" />
      </li>
    </ul>
  </div>
  
</form></div></body>

</html>
