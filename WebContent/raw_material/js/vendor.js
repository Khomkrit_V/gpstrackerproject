
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=700,width=800,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
function new_vendor(){
	document.getElementById("vendor_pointer_id").value="nothing";
	vendor_pointer_change();
	document.getElementById("v_name").value="";
	document.getElementById("v_contact_name").value="";
	document.getElementById("v_phone").value="";
	document.getElementById("v_fax").value="";
	document.getElementById("v_email").value="";
	document.getElementById("v_website").value="";
	document.getElementById("v_pay_method").value="";
	document.getElementById("v_tax").value="";
	document.getElementById("v_tran").value="";
	document.getElementById("v_remark").value="";	
	try {
	    var table = document.getElementById("table_v_p1");
	    var rowCount = table.rows.length;

	    for(var i=1; i<rowCount; i++) {       
	            table.deleteRow(i);
	            rowCount--;
	            i--;
	            }
	    rowCount = table.rows.length;
	    row = table.insertRow(rowCount);
	    
	    var cell1 = row.insertCell(0);
	    cell1.innerHTML="<div id=\"tr_"+rowCount+"_0\" style=\"height: 20px;\" onclick=\"add_vendor_p()\">*</div>";

	    var cell2 = row.insertCell(1);
	    cell2.innerHTML = "<div id=\"tr_"+rowCount+"_1\" style=\"height: 20px;\"></div>";

	    var cell3 = row.insertCell(2);
	    cell3.innerHTML = "<div id=\"tr_"+rowCount+"_2\" style=\"height: 20px;\"></div>";
	    
	    }catch(e) {
	        alert(e);
	    }
	
}
function vendor_pointer_change(){
	try{
	var pointer = document.getElementById("vendor_pointer_id").value;
    var xmlhttp;		 
    
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	  xmlhttp.onreadystatechange=function()
	  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		  {
			  xmlDoc=xmlhttp.responseXML;			
			  var x1 =xmlDoc.getElementsByTagName("VNAME");
			  document.getElementById("vendor_pointer_id").value=x1[0].childNodes[0].nodeValue;
			  document.getElementById("v_name").value=x1[0].childNodes[0].nodeValue;
			  var x2 =xmlDoc.getElementsByTagName("VCONNAME");
			  document.getElementById("v_contact_name").value=x2[0].childNodes[0].nodeValue;
			  var x3 =xmlDoc.getElementsByTagName("VPHONE");
			  document.getElementById("v_phone").value=x3[0].childNodes[0].nodeValue;
			  var x4 =xmlDoc.getElementsByTagName("VFAX");
			  document.getElementById("v_fax").value=x4[0].childNodes[0].nodeValue;
			  var x5 =xmlDoc.getElementsByTagName("VEMAIL");
			  document.getElementById("v_email").value=x5[0].childNodes[0].nodeValue;
			  var x6 =xmlDoc.getElementsByTagName("VWEBSITE");
			  document.getElementById("v_website").value=x6[0].childNodes[0].nodeValue;
			  var x7 =xmlDoc.getElementsByTagName("VADDRESS");
			  document.getElementById("v_address").value=x7[0].childNodes[0].nodeValue;
			  var x8 =xmlDoc.getElementsByTagName("VPAYMETHOD");
			  document.getElementById("v_pay_method").value=x8[0].childNodes[0].nodeValue;
			  var x9 =xmlDoc.getElementsByTagName("VTRAN");
			  document.getElementById("v_tran").value=x9[0].childNodes[0].nodeValue;
			  var x10 =xmlDoc.getElementsByTagName("VCURRENCY");
			  document.getElementById("v_currency").value=x10[0].childNodes[0].nodeValue;
			  var x11 =xmlDoc.getElementsByTagName("VTAX");
			  document.getElementById("v_tax").value=x11[0].childNodes[0].nodeValue;
			  var x11 =xmlDoc.getElementsByTagName("VREMARK");
			  document.getElementById("v_remark").value=x11[0].childNodes[0].nodeValue;
			  
	     
		  }
	  };
	  xmlhttp.open("POST","../utilJS",true);		
	  xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	  xmlhttp.send("param=vendor_pointer_change&v_id="+pointer);
	}catch(e){
		alert(e);
	}
}
function vendor_name_change(){
	try{
	var pointer = document.getElementById("v_name").value;
    var xmlhttp;		 
    
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	  xmlhttp.onreadystatechange=function()
	  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		  {
			  xmlDoc=xmlhttp.responseXML;			
			  var x1 =xmlDoc.getElementsByTagName("VNAME");
			  document.getElementById("vendor_pointer_id").value=x1[0].childNodes[0].nodeValue;
			  document.getElementById("v_name").value=x1[0].childNodes[0].nodeValue;
			  var x2 =xmlDoc.getElementsByTagName("VCONNAME");
			  document.getElementById("v_contact_name").value=x2[0].childNodes[0].nodeValue;
			  var x3 =xmlDoc.getElementsByTagName("VPHONE");
			  document.getElementById("v_phone").value=x3[0].childNodes[0].nodeValue;
			  var x4 =xmlDoc.getElementsByTagName("VFAX");
			  document.getElementById("v_fax").value=x4[0].childNodes[0].nodeValue;
			  var x5 =xmlDoc.getElementsByTagName("VEMAIL");
			  document.getElementById("v_email").value=x5[0].childNodes[0].nodeValue;
			  var x6 =xmlDoc.getElementsByTagName("VWEBSITE");
			  document.getElementById("v_website").value=x6[0].childNodes[0].nodeValue;
			  var x7 =xmlDoc.getElementsByTagName("VADDRESS");
			  document.getElementById("v_address").value=x7[0].childNodes[0].nodeValue;
			  var x8 =xmlDoc.getElementsByTagName("VPAYMETHOD");
			  document.getElementById("v_pay_method").value=x8[0].childNodes[0].nodeValue;
			  var x9 =xmlDoc.getElementsByTagName("VTRAN");
			  document.getElementById("v_tran").value=x9[0].childNodes[0].nodeValue;
			  var x10 =xmlDoc.getElementsByTagName("VCURRENCY");
			  document.getElementById("v_currency").value=x10[0].childNodes[0].nodeValue;
			  var x11 =xmlDoc.getElementsByTagName("VTAX");
			  document.getElementById("v_tax").value=x11[0].childNodes[0].nodeValue;
			  var x11 =xmlDoc.getElementsByTagName("VREMARK");
			  document.getElementById("v_remark").value=x11[0].childNodes[0].nodeValue;
			  
	     
		  }
	  };
	  xmlhttp.open("POST","../utilJS",true);		
	  xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	  xmlhttp.send("param=vendor_pointer_change&v_id="+pointer);
	}catch(e){
		alert(e);
	}
}
function vendor_save(){
	try{
	var pointer = document.getElementById("vendor_pointer_id").value;
	var n1=document.getElementById("v_name").value;	 
	  var n2=document.getElementById("v_contact_name").value;	 
	  var n3=document.getElementById("v_phone").value;
	  var n4=document.getElementById("v_fax").value;
	  var n5=document.getElementById("v_email").value;	
	  var n6=document.getElementById("v_website").value;
	  var n7=document.getElementById("v_address").value;	
	  var n8=document.getElementById("v_pay_method").value;	
	  var n9=document.getElementById("v_tran").value;
	  var n10=document.getElementById("v_currency").value;
	  var n11=document.getElementById("v_tax").value;	 
	  var n12=document.getElementById("v_remark").value;
    var xmlhttp;		 
    
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	  xmlhttp.onreadystatechange=function()
	  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		  {
			  xmlDoc=xmlhttp.responseXML;			
			  var x1 =xmlDoc.getElementsByTagName("VADDSTAT");
			 var f1=x1[0].childNodes[0].nodeValue;
			 if(f1=="yes"){
				 alert("Add success");
			 }else{
				 alert("Add Failed");
			 }
			  
			  
	     
		  }
	  };
	  xmlhttp.open("POST","../utilJS",true);		
	  xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	  xmlhttp.send("param=vendor_save&v_name="+n1+"&v_con_name="+n2+
			  "&v_phone="+n3+"&v_fax="+n4+"&v_email="+n5+
			  "&v_website="+n6+"&v_address="+n7+"&v_pay_method="+n8+
			  "&v_tran="+n9+"&v_currency="+n10+"&v_tax="+n11+"&v_remark="+n12);
	}catch(e){
		alert(e);
	}
}
function search_vendor(){
	try{		
	    var xmlhttp;		 
	    
		  if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
		  }
		  else
		  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		  xmlhttp.onreadystatechange=function()
		  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			  {
				  xmlDoc=xmlhttp.responseXML;			
				  var x1 =xmlDoc.getElementsByTagName("VNAME");
				  var row1 =xmlDoc.getElementsByTagName("ROW");
				 var f1=x1[0].childNodes[0].nodeValue;
				 var row=row1[0].childNodes[0].nodeValue;
				 for(var i=0;i<row;i++){
					 alert(x1[i].childNodes[0].nodeValue);
				 }				 
				  
				  
		     
			  }
		  };
		  xmlhttp.open("POST","../utilJS",true);		
		  xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		  xmlhttp.send("param=find_dat&collName=vendor");
		}catch(e){
			alert(e);
		}
}
