<%@page import="getdat.getDatabase"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="java.util.*,com.mongodb.*" %>
    <%    
    HttpSession ses = request.getSession();
    String usn=(String) ses.getAttribute("usn");
	String pwd=(String) ses.getAttribute("pwd");
	String user_stat=(String) ses.getAttribute("user_stat");
	String company_id=(String) ses.getAttribute("company_id");
	DBObject[] vehicle_data =null;
	int rowcount = getDatabase.getRowCount("vehicle", "Company_ID", company_id);
	vehicle_data = getDatabase.getCollData("vehicle", "Company_ID", company_id, rowcount,true,"Vehicle_ID",1);
	ses.setAttribute("row", ""+rowcount);
	
    %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html><head>
<meta name="XAR Files" content="index_htm_files/xr_files.txt"/>
 <title>Add Deliver</title>
 <meta http-equiv="Content-Type" content="text/html; charset=Windows-874"/>
 <meta name="Generator" content="Xara HTML filter v.4.0.4.653"/>
 <link rel="stylesheet" type="text/css" href="index_htm_files/xr_main.css"/>
 <link rel="stylesheet" type="text/css" href="index_htm_files/xr_text.css"/>
 <script type="text/javascript" src="index_htm_files/roe.js"></script>
  <script type="text/javascript"
	src="./js/util.js">
	</script>
<meta http-equiv="Content-Type" content="text/html;" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="HandheldFriendly" content="true" />
<title>Form</title>
<link href="css/FormCss_1.css" rel="stylesheet" type="text/css" />
<link href="css/myCustom.css" rel="stylesheet" type="text/css" />
<link type="text/css" rel="stylesheet" href="css/FormCss_2.css" />
<style type="text/css">
    .form-label{
        width:150px !important;
    }
    .form-label-left{
        width:150px !important;
    }
    .form-line{
        padding-top:12px;
        padding-bottom:12px;
    }
    .form-label-right{
        width:150px !important;
    }
    body, html{
        margin:0;
        padding:0;
        background:false;
    }

    .form-all{
        margin:0px auto;
        padding-top:20px;
        width:1360px;
        color:#555 !important;
        font-family:'Lucida Grande';
        font-size:14px;
    }
    .form-radio-item label, .form-checkbox-item label, .form-grading-label, .form-header{
        color:#555;
    }

</style>
<script src="js/FormJS_1.JS" type="text/javascript"></script>
<script type="text/javascript">
   JotForm.init();   
</script>
</head>
<body>
<label class="nor_text" id="he1" >
          Add Group
        </label>
<!--[if IE]><div class="xr_ap" id="xr_xri" style="width: 1360px; height: 800px; clip: rect(0px 1360px 800px 0px);"><![endif]-->
<!--[if !IE]>--><div class="xr_ap" id="xr_xri" style="width: 1360px; height: 800px; clip: rect(0px, 1360px, 800px, 0px);"><!--<![endif]-->
 <img class="xr_ap" src="index_htm_files/9.png" alt="" title="" style="left: 0px; top: 0px; width: 1360px; height: 796px;"/>
 <div class="edit_page">
 <form id="add_group" name="add_group" method="post" action="E_edit?task=group&dd=0&ins=1">
 <div class="addgroup_name">
 <label class="form-label-left" id="group_name2" for="group_name">
         Group Name:<span class="form-required">*</span>
        </label>
        <div id="group_name1" class="form-input">         
 		<input class="form-textbox validate[required]" type="text" id="group_name" name="group_name">
		 </div>
 </div>
 <div class="addgroup_list">
 <%
 for(int i=1;i<=rowcount;i++){
	 out.println("<input type=\"checkbox\" name=\""+vehicle_data[i].get("Vehicle_ID")+"\" value=\""+vehicle_data[i].get("Vehicle_ID")+"\">"+vehicle_data[i].get("Vehicle_ID")+" "+vehicle_data[i].get("RegisterID")+" "+vehicle_data[i].get("Brand")+" "+vehicle_data[i].get("Model")+" "+vehicle_data[i].get("Color")+"<br>");
 }
 %> 
 </div>
 <div class="addgroup_submit">
 <input type="button" value="Add Group" onclick="validateForm_addgroup()">
 </div>
 </form>
 </div>
 </div></body>

</html>
